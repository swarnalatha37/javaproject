package com.Package;
//parent class
public class Father {
	public Father()  // father class constructor
	{
		System.out.println("in father constructor");
}
	private int bankpin;
	private void useMoney()   // same class can use
	{
		System.out.println("in  private use money ");
	}
	protected void useHouse( )  // same and child class can use
	{
		System.out.println("in  protected can use house ");
	}
	public void seeHouse()
	{

	}

}
