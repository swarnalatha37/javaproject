package com.Package;
import java.util.Scanner;
public class Fristprgrm {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);

		  // Scanner is class name
		  // s is object name
		 //new - keywoard - allocate a memory for that object
		 // System.in - keyboard

		 int no1;
		float f1;
		System.out.println("Enter a int value");
		no1 = s.nextInt();

		  //nextInt is to take a integer value
		  // nextInt - method name

		System.out.println("Entered value is" +no1);
		 
		System.out.println("Enter a float value");
		f1 = s.nextFloat();


		//nextFloat is to take a Float value
		  // nextFloat - method name

		 System.out.println("Entered value is" +f1);

		String s1;
		System.out.println("Enter a string value");
		s1 = s.next();
		System.out.println("Entered value is" +s1);

	}

}
