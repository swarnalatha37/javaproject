package com.Package;
import java.util.Scanner;
public class SortArray {

	public static void main(String[] args) {
		int n,i,j,temp;
	    Scanner s = new Scanner(System.in);
	 
	    System.out.println("enter number of integers to sort");
	    n = s.nextInt();
	 
	    int array[] = new int[5];
	 
	    System.out.println("Enter  integers");
	 
	    for (i = 0; i < 5; i++)
	      array[i] = s.nextInt();
	   
	    for (i = 0; i < ( 5 - 1 ); i++) {
	      for (j = 0; j < 5- i - 1; j++) {
	        if (array[j] > array[j+1])
	        {
	          temp       = array[j];
	          array[j]   = array[j+1];
	          array[j+1] = temp;
	        }
	      }
	    }
	 
	    System.out.println("Sorted list of numbers:");
	 
	    for (i = 0; i < 5; i++)
	      System.out.println(array[i]);
	  }
}

