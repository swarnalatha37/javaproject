package com.Package;
import java.util.Scanner;
public class Square {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
        System.out.println("enter  side length value: ");
        double val = s.nextDouble();
        System.out.println("Square %12.2f", val * val);
        System.out.println("Cube %14.2f", val * val * val);
        System.out.println("Fourth power %6.2f", Math.pow(val, 4));
	     }
	}